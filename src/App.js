import "./styles/App.css";
import Header from "./components/Header";
import Card from "./components/Card";
import { Component } from "react";
import Button from "./components/Button";
import Loading from "./components/Loading";

class App extends Component {
  state = {
    jobs: [],
    loading: true,
    technologys: ["javascript", "python", "react"],
    description: "",
  };

  pickTechnology = (technology) => {
    console.log("chamou");
    this.setState({
      loading: true,
      description: technology,
    });
  };

  componentDidMount() {
    fetch("https://jobhunt-api.herokuapp.com/jobs")
      .then((response) => response.json())
      .then((response) =>
        this.setState({
          jobs: response,
          loading: false,
        })
      );
  }

  componentDidUpdate() {
    fetch(
      `https://jobhunt-api.herokuapp.com/jobs?description=${this.state.description}`
    )
      .then((response) => response.json())
      .then((response) =>
        this.setState({
          jobs: response,
          loading: false,
        })
      );
  }

  render() {
    const { jobs, loading, technologys } = this.state;
    return (
      <div className="App">
        <Header></Header>
        <h2>🔥 Tranding Jobs</h2>
        <div className="pickList">
          <button
            className="pickTec"
            onClick={() => {
              this.pickTechnology("");
            }}
          >
            Show All
          </button>
          {technologys.map((technology) => (
            <Button onClickFunc={this.pickTechnology} technology={technology} />
          ))}
        </div>
        {loading ? (
          <Loading />
        ) : (
          <div className="cardList">
            {jobs.map((job) => (
              <Card job={job} />
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default App;

import { Component } from "react";
import Logo from "../../assets/logo.png";
import "./style.css";

class Header extends Component {
  render() {
    return (
      <header className="header-container">
        <img src={Logo} alt="" />
      </header>
    );
  }
}

export default Header;

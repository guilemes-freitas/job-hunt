import { Component } from "react";
import "./style.css";

class Button extends Component {
  render() {
    const { technology, onClickFunc } = this.props;
    return (
      <button
        className="pickTec"
        onClick={() => {
          onClickFunc(technology);
        }}
      >
        {technology}
      </button>
    );
  }
}

export default Button;

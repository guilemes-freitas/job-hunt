import { gsap, TweenMax } from "gsap";
import "./style.css";
import { useRef, useEffect } from "react";

function Loading() {
  const ball = useRef(null);
  const balls = useRef([]);
  balls.current = [];

  const addRefs = (el) => {
    if (el && !balls.current.includes(el)) {
      balls.current.push(el);
    }
  };
  useEffect(() => {
    TweenMax.staggerFromTo(
      balls.current,
      1,
      {
        scale: 0.1,
        opacity: 0,
      },
      {
        scale: 1.2,
        opacity: 1,
        repeat: -1,
        yoyo: true,
      },
      0.2
    );
  }, []);

  return (
    <div>
      <span className="ball" ref={addRefs}></span>
      <span className="ball" ref={addRefs}></span>
      <span className="ball" ref={addRefs}></span>
      <span className="ball" ref={addRefs}></span>
      <span className="ball" ref={addRefs}></span>
    </div>
  );
}

export default Loading;
